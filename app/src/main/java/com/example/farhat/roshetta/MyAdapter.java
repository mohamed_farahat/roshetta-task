package com.example.farhat.roshetta;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private List<item> items;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView pharmacyName, medicinName, medicinSubName, discountPercentage, price, packetPrice;

        public MyViewHolder(View view) {
            super(view);
            pharmacyName = view.findViewById(R.id.pharmcy_name);
            medicinName = view.findViewById(R.id.medicin_name);
            medicinSubName = view.findViewById(R.id.medicin_subName);
            discountPercentage = view.findViewById(R.id.discount_percentage);
            price = view.findViewById(R.id.price);
            packetPrice = view.findViewById(R.id.packet_price);

        }
    }


    public MyAdapter(List<item> items) {
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        item item = items.get(position);
        holder.pharmacyName.setText(item.getPharmName());
        holder.medicinName.setText(item.getMedName());
        holder.medicinSubName.setText(item.getMedSubName());
        holder.discountPercentage.setText(item.getPersentage());
        holder.price.setText(item.getPrice());
        holder.packetPrice.setText(item.getPackPrice());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}