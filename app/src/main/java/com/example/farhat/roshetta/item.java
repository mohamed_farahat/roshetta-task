package com.example.farhat.roshetta;

public class item {
    String pharmName;
    String medName;
    String medSubName;
    String persentage;
    String price;
    String packPrice;

    public item(String pharmName, String medName, String medSubName, String persentage, String price, String packPrice) {
        this.pharmName = pharmName;
        this.medName = medName;
        this.medSubName = medSubName;
        this.persentage = persentage;
        this.price = price;
        this.packPrice = packPrice;
    }

    public String getPharmName() {
        return pharmName;
    }

    public void setPharmName(String pharmName) {
        this.pharmName = pharmName;
    }

    public String getMedName() {
        return medName;
    }

    public void setMedName(String medName) {
        this.medName = medName;
    }

    public String getMedSubName() {
        return medSubName;
    }

    public void setMedSubName(String medSubName) {
        this.medSubName = medSubName;
    }

    public String getPersentage() {
        return persentage;
    }

    public void setPersentage(String persentage) {
        this.persentage = persentage;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPackPrice() {
        return packPrice;
    }

    public void setPackPrice(String packPrice) {
        this.packPrice = packPrice;
    }
}
